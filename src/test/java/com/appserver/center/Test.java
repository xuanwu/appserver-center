package com.appserver.center;

import org.apache.commons.lang.StringUtils;

import com.sprouts.spm_framework.utils.HTTPUtils;

public class Test {

    public static void main(String[] args) throws Exception {
        String stopUrl = "http://localhost:8080/manager/stop?path=/examples";
        String listUrl = "http://localhost:8080/manager/text/list";
        String result = HTTPUtils.doGet(listUrl, "appserver");
        // String result = HTTPUtils.doPost(listUrl);
        String[] resultSet = StringUtils.split(result, "/");

        System.out.println(resultSet.length);
        for (String str : resultSet) {
            System.out.println(str);
        }

    }
}
