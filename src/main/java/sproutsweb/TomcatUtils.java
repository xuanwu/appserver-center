package sproutsweb;

import com.sprouts.spm_framework.utils.HTTPUtils;
import com.sprouts.spm_framework.utils.Logger;

public class TomcatUtils implements WebAppsInterface {

    private Logger logger = new Logger();

    @Override
    public boolean deploy(String ip, int port, String webapp, String localFilePath) {
        String url =
                "http://" + ip + ":" + port + "/manager/text/install?path=/" + webapp
                        + "&war=jar:file:/" + localFilePath + "!/";
        return getResult(url);
    }

    @Override
    public boolean redeploy(String ip, int port, String webapp) {
        String url = "http://" + ip + ":" + port + "/manager/text/reload?path=/" + webapp;
        return getResult(url);
    }

    @Override
    public boolean listApps(String ip, int port) {
        String url = "http://" + ip + ":" + port + "/manager/text/list";
        return getResult(url);
    }

    @Override
    public boolean startApp(String ip, int port, String webapp) {
        String url = "http://" + ip + ":" + port + "/manager/text/start?path=/" + webapp;
        return getResult(url);
    }

    @Override
    public boolean stopApp(String ip, int port, String webapp) {
        String url = "http://" + ip + ":" + port + "/manager/text/stop?path=/" + webapp;
        return getResult(url);
    }

    public boolean getResult(String url) {
        try {
            String result = HTTPUtils.doGet(url, "tomcat");
            if (result.startsWith("OK")) {
                return true;
            }
        } catch (Exception e) {
            logger.error("", e);
        }
        return false;
    }
}
