package sproutsweb;

public interface WebAppsInterface {

    public boolean deploy(String ip, int port, String webapp, String localFilePath);

    public boolean redeploy(String ip, int port, String webapp);

    public boolean listApps(String ip, int port);

    public boolean startApp(String ip, int port, String webapp);

    public boolean stopApp(String ip, int port, String webapp);

}
