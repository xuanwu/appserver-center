package com.appserver.utils;

public class AppSerConst {

    public static String LIST_APPSERVER_TABLE = "list_spm_appserver";
    public static String COLLECT_APPSERVER_TABLE = "collect_appserver";
    public static String CONFIG_PATH = "../config/appserver-config.xml";

    public static String NIO_SERVER_PORT = "AppServer.nio.server-port";
    public static String NIO_CLIENT_PORT = "AppServer.nio.client-port";
}
