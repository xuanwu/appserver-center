package com.appserver.center;

import com.appserver.handler.DataReceiver;
import com.appserver.receiver.AmqTaskReceiver;
import com.appserver.utils.AppSerConst;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.ConfigUtils;

public class AppServerCenter {

    static AsyncTaskUtils asyncTaskHandler = AsyncTaskUtils.getInstance();

    public static void main(String[] args) {

        // 初始化整个程序的配置文件
        ConfigUtils.initExternalConfig(AppSerConst.CONFIG_PATH);


        // 启动数据接收器
        DataReceiver dataReceiver = new DataReceiver();
        asyncTaskHandler.dispatchTask(dataReceiver);


        // 启动任务接收器
        AmqTaskReceiver taskReceiver = new AmqTaskReceiver();
        taskReceiver.initReceiver();

    }
}
