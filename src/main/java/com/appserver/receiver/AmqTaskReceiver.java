package com.appserver.receiver;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.appserver.handler.CacheThread;
import com.appserver.handler.TaskDispatcher;
import com.appserver.pojo.AppSerTask;
import com.appserver.service.CachedService;
import com.sprouts.spm_framework.amq.AmqConfig;
import com.sprouts.spm_framework.amq.AmqReceiver;
import com.sprouts.spm_framework.amq.message.AppSerMessage;
import com.sprouts.spm_framework.utils.AsyncTaskUtils;
import com.sprouts.spm_framework.utils.ConfigUtils;
import com.sprouts.spm_framework.utils.Logger;
import com.sprouts.spm_framework.utils.SPMConstants;

/**
 * 接收任务
 * 
 * @author howson
 * 
 */
public class AmqTaskReceiver {
    public static AmqReceiver receiver;
    private Logger logger = new Logger();
    AsyncTaskUtils asynHandler = null;

    public void initReceiver() {

        asynHandler = AsyncTaskUtils.getInstance();
        AmqConfig config =
                AmqConfig.getDefaultConfig(ConfigUtils.getValue(SPMConstants.APP_SER_BROKER),
                        ConfigUtils.getValue(SPMConstants.APP_SER_QUEUE), AppSerMessage.class,
                        new AppSerTasksListener());
        receiver = AmqReceiver.initAmqReceiver(config);
        receiver.receive();

        asynHandler.dispatchScheduleTask(new CacheThread(), 5, 1);// 启动定时缓存线程
        logger.info("The AmqTaskReceiver startup!");
    }

    public class AppSerTasksListener implements MessageListener {

        @Override
        public void onMessage(Message arg0) {
            if (arg0 != null) {
                AppSerMessage message = new AppSerMessage();
                message = (AppSerMessage) message.parseMapMsgToBaseMsg((MapMessage) arg0);
                AppSerTask task =
                        new AppSerTask(message.getHostName(), message.getPort(), message.getType(),
                                message.getFrequency(), message.getCenterMachine());
                if (message.getTaskType() == 0) {
                    TaskDispatcher nioClient = new TaskDispatcher(task, 0);
                    asynHandler.dispatchTask(nioClient);
                    CachedService.removeTask(task);
                } else if (message.getTaskType() == 2) {
                    TaskDispatcher nioClient = new TaskDispatcher(task, 2);
                    asynHandler.dispatchTask(nioClient);
                    CachedService.putToTaskQueue(task);
                }
            }
        }
    }

}
