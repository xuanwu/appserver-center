package com.appserver.pojo;

import java.io.Serializable;

public class AppSerStatus implements Serializable{

	private static final long serialVersionUID = 1L;

	private Double cpu_usage;
	
	private String host;
	
	private Integer port;
	
	private String time_current;
	
	private Integer alive_thread;

	private Float mem_usage;
	
	private long cpu_time;
	
	private long total_thread;
	
	private Integer id;
	
	public Double getCpu_usage() {
		return cpu_usage;
	}

	public void setCpu_usage(Double cpu_usage) {
		this.cpu_usage = cpu_usage;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getTime_current() {
		return time_current;
	}

	public void setTime_current(String time_current) {
		this.time_current = time_current;
	}

	public Integer getAlive_thread() {
		return alive_thread;
	}

	public void setAlive_thread(Integer alive_thread) {
		this.alive_thread = alive_thread;
	}

	public Float getMem_usage() {
		return mem_usage;
	}

	public void setMem_usage(Float mem_usage) {
		this.mem_usage = mem_usage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getCpu_time() {
		return cpu_time;
	}

	public void setCpu_time(long cpu_time) {
		this.cpu_time = cpu_time;
	}

	public long getTotal_thread() {
		return total_thread;
	}

	public void setTotal_thread(long total_thread) {
		this.total_thread = total_thread;
	}

	@Override
	public String toString() {
		return "ASStatus [cpu_usage=" + cpu_usage + ", host=" + host
				+ ", port=" + port + ", time_current=" + time_current
				+ ", alive_thread=" + alive_thread + ", mem_usage=" + mem_usage
				+ ", cpu_time=" + cpu_time + ", total_thread=" + total_thread
				+ ", id=" + id + "]";
	}

	
}
