package com.appserver.pojo;

import java.io.Serializable;

public class AppSerTask implements Serializable {

    private static final long serialVersionUID = 1L;

    private String hostname;
    private int port;
    private String type;
    private int frequency;
    private String center_machine;

    public AppSerTask() {}

    public AppSerTask(String hostname, int port, String type, int frequency, String center_machine) {
        this.hostname = hostname;
        this.port = port;
        this.type = type;
        this.frequency = frequency;
        this.center_machine = center_machine;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getCenter_machine() {
        return center_machine;
    }

    public void setCenter_machine(String center_machine) {
        this.center_machine = center_machine;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + frequency;
        result = prime * result + ((hostname == null) ? 0 : hostname.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + port;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        AppSerTask other = (AppSerTask) obj;
        if (frequency != other.frequency) return false;
        if (hostname == null) {
            if (other.hostname != null) return false;
        } else if (!hostname.equals(other.hostname)) return false;
        if (type == null) {
            if (other.type != null) return false;
        } else if (!type.equals(other.type)) return false;
        if (port != other.port) return false;
        return true;
    }

    @Override
    public String toString() {
        return "ASTask [hostname=" + hostname + ", port=" + port + ", type=" + type
                + ", frequency=" + frequency + ", center_machine=" + center_machine + "]";
    }

}
