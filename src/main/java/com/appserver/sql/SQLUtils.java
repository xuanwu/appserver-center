package com.appserver.sql;

import java.sql.SQLException;

import com.appserver.pojo.AppSerStatus;
import com.sprouts.spm_framework.sql.SQLService;

public class SQLUtils extends SQLService {

    public void insert(AppSerStatus status) {
        reInitService();
        String insertSql = "insert into collect_appserver values(NULL,?,?,?,?,?,?,?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(insertSql);
            preparedStatement.setInt(1, status.getAlive_thread());
            preparedStatement.setLong(2, status.getTotal_thread());
            preparedStatement.setFloat(3, status.getMem_usage());
            preparedStatement.setLong(4, status.getCpu_time());
            preparedStatement.setDouble(5, status.getCpu_usage());
            preparedStatement.setString(6, status.getHost());
            preparedStatement.setInt(7, status.getPort());
            preparedStatement.setLong(8, getTimestampSec());
            preparedStatement.setString(9, getTimeStamp());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
