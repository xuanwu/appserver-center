package com.appserver.handler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import com.appserver.pojo.AppSerStatus;
import com.appserver.sql.SQLUtils;
import com.appserver.utils.AppSerConst;
import com.appserver.utils.NioHandler;
import com.sprouts.spm_framework.utils.ConfigUtils;
import com.sprouts.spm_framework.utils.Logger;

public class DataReceiver implements Runnable {

    private InetSocketAddress inetSocketAddress;
    private NioHandler handler = new ServerHandler();
    private SQLUtils sqlService = null;
    private Logger logger = new Logger();

    public DataReceiver() {
        inetSocketAddress =
                new InetSocketAddress(Integer.parseInt(ConfigUtils
                        .getValue(AppSerConst.NIO_SERVER_PORT)));
        this.sqlService = new SQLUtils();
    }

    public void run() {
        try {
            Selector selector = Selector.open(); // 打开选择器
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open(); // 打开通道
            serverSocketChannel.configureBlocking(false); // 非阻塞
            serverSocketChannel.socket().bind(inetSocketAddress);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT); // 向通道注册选择器和对应事件标识
            logger.info("Server: socket server started.");
            while (true) { // 轮询
                int nKeys = selector.select();
                if (nKeys > 0) {
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    Iterator<SelectionKey> it = selectedKeys.iterator();
                    while (it.hasNext()) {
                        SelectionKey key = it.next();
                        if (key.isAcceptable()) {
                            logger.info("Server: SelectionKey is acceptable.");
                            handler.handleAccept(key);
                        } else if (key.isReadable()) {
                            logger.info("Server: SelectionKey is readable.");
                            handler.handleRead(key);
                        } else if (key.isWritable()) {
                            logger.info("Server: SelectionKey is writable.");
                            handler.handleWrite(key);
                        }
                        it.remove();
                    }
                }
            }
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    /**
     * 服务端事件处理实现类
     */
    class ServerHandler implements NioHandler {

        public void handleAccept(SelectionKey key) throws IOException {
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
            SocketChannel socketChannel = serverSocketChannel.accept();
            logger.info("Server: accept client socket " + socketChannel);
            System.out.println("center accepted from worker!");
            socketChannel.configureBlocking(false);
            socketChannel.register(key.selector(), SelectionKey.OP_READ);
        }


        public void handleRead(SelectionKey key) throws IOException {
            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
            SocketChannel socketChannel = (SocketChannel) key.channel();
            while (true) {
                int readBytes = socketChannel.read(byteBuffer);
                if (readBytes > 0) {
                    String taskdata = new String(byteBuffer.array(), 0, readBytes);
                    if (!taskdata.isEmpty()) {
                        ByteArrayInputStream input = new ByteArrayInputStream(byteBuffer.array());
                        ObjectInputStream objInput = new ObjectInputStream(input);
                        AppSerStatus status = null;
                        try {
                            status = (AppSerStatus) objInput.readObject();
                        } catch (ClassNotFoundException e) {
                            logger.error("", e);
                        }

                        sqlService.insert(status);
                        socketChannel.write(ByteBuffer.wrap("success,ser".getBytes()));
                        break;
                    } else {
                        logger.warn("Collect data is empty.");
                        socketChannel.write(ByteBuffer.wrap("failed,ser".getBytes()));
                        break;
                    }
                }
            }
            socketChannel.close();
        }

        public void handleWrite(SelectionKey key) throws IOException {
            ByteBuffer byteBuffer = (ByteBuffer) key.attachment();
            byteBuffer.flip();
            SocketChannel socketChannel = (SocketChannel) key.channel();
            socketChannel.write(byteBuffer);
            if (byteBuffer.hasRemaining()) {
                key.interestOps(SelectionKey.OP_READ);
            }
            byteBuffer.compact();
        }
    }

}
