package com.appserver.handler;

import com.appserver.service.CachedService;

/**
 * 定时检查队列并把队列元素写到本地持久化文件中
 * 
 * @author howson
 * 
 */
public class CacheThread implements Runnable {

    @Override
    public void run() {
        CachedService.cachedToLocal();
    }

}
