package com.appserver.handler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import com.appserver.pojo.AppSerTask;
import com.appserver.utils.AppSerConst;
import com.sprouts.spm_framework.utils.ConfigUtils;
import com.sprouts.spm_framework.utils.Logger;

/**
 * 使用nio派发任务，实际上是一个nio socket的client
 * 
 * @author howson
 * 
 */
public class TaskDispatcher implements Runnable {

    private InetSocketAddress inetSocketAddress;
    private AppSerTask task = null;
    private int taskType; // 任务类型，0是取消监控,1是心跳包,2是创建新监控
    private Logger logger = new Logger();

    public TaskDispatcher(AppSerTask task, int taskType) {
        this.task = task;
        inetSocketAddress =
                new InetSocketAddress(task.getHostname(), Integer.parseInt(ConfigUtils
                        .getValue(AppSerConst.NIO_CLIENT_PORT)));
        this.taskType = taskType;
    }

    public void run() {
        String request =
                taskType + "*" + task.getHostname() + "*" + task.getPort() + "*" + task.getType()
                        + "*" + task.getFrequency() + "*" + task.getCenter_machine();
        send(request);
    }

    public void send(String requestData) {
        try {
            SocketChannel socketChannel = SocketChannel.open(inetSocketAddress);
            socketChannel.configureBlocking(false);
            ByteBuffer byteBuffer = ByteBuffer.allocate(512);
            socketChannel.write(ByteBuffer.wrap(requestData.getBytes()));
            while (true) {
                byteBuffer.clear();
                int readBytes = socketChannel.read(byteBuffer);
                if (readBytes > 0) {
                    byteBuffer.flip();
                    logger.info("Client: data = " + new String(byteBuffer.array(), 0, readBytes));
                    socketChannel.close();
                    break;
                }
            }

        } catch (IOException e) {
            logger.error("send exception:" + e);
        }
    }
}
